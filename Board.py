class Board:
    #   p = pawn
    #   kg = king
    #   q = queen
    #   r = rook
    #   b = bishop
    #   k = knight
    #   0 = empty
    def __init__(self):
        self.board_state = [
            [1, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
        ]

    def __repr__(self):
        return f"{self.board_state}"

    def get_state(self, x, y):
        """Return state at given x and y coords"""
        return self.board_state[x][y]

    def set_state(self, x, y, new_state):
        """Set new state on x and y coords with new_state"""
        self.board_state[x][y] = new_state


if __name__ == "__main__":
    print("Board")
    b = Board()
    print(b)
