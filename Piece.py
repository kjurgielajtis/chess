class Piece:
    def __init__(self, color, symbol):
        self.pos = [0, 0]
        self.color = color
        self.symbol = symbol
        self.unicode = self.get_unicode(color, symbol)

    def __repr__(self):
        return f"{self.unicode}"

    def get_unicode(self, color, symbol):
        """
        Return unicode symbol of piece according to arguments.

        Keyword arguments:
        color -- determinces color of a piece
        symbol -- determines type of a piece
        """
        select_unicode = {
            "white": {
                "king": u"♔",
                "queen": u"♕",
                "rook": u"♖",
                "bishop": u"♗",
                "knight": u"♘",
                "pawn": u"♙"
            },

            "black": {
                "king": u"♚",
                "queen": u"♛",
                "rook": u"♜",
                "bishop": u"♝",
                "knight": u"♞",
                "pawn": u"♟︎"
            }
        }
        return select_unicode[color][symbol]

    def set_pos(self, x, y):
        self.pos = [x, y]

    def get_pos(self):
        return self.pos

    def set_color(self, color):
        self.color = color

    def get_color(self):
        return self.color

    def set_symbol(self, symbol):
        self.symbol = symbol

    def get_symbol(self):
        return self.symbol


if __name__ == "__main__":
    print(Piece)
